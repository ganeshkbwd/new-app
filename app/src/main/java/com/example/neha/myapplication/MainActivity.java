package com.example.neha.myapplication;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.neha.myapplication.database.DatabaseHelper;
import com.example.neha.myapplication.model.ImageData;
import com.example.neha.myapplication.utility.CheckConnectivity;
import com.example.neha.myapplication.utility.PermissionUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    String message_code;
    List<Integer> catIdList = new ArrayList<Integer>();
    List<String> titleList = new ArrayList<String>();
    List<String> descriptionList = new ArrayList<String>();
    List<String> catImageList = new ArrayList<String>();
    List<ImageData> imageDataList = new ArrayList<ImageData>();
    DatabaseHelper db;
    ImageData imageData;
    PermissionUtility putility;
    ArrayList<String> permission_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DatabaseHelper(this);
        putility = new PermissionUtility(this);
        permission_list = new ArrayList<String>();
//        permission_list.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permission_list.add(android.Manifest.permission.INTERNET);
        permission_list.add(android.Manifest.permission.ACCESS_WIFI_STATE);
        permission_list.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
        toolbar();
        callWebService();

    }

    public void handler() {
        new Handler().postDelayed(new Runnable() {
            // Using handler with postDelayed called runnable run method
            @Override
            public void run() {
                Intent i = new Intent(MainActivity.this, WelcomeActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, 3 * 1000); // wait for 5 seconds
    }

    public void toolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
//        changeStatusBarColor();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.statusBarColor));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void callWebService() {
        if (CheckConnectivity.checkInternetConnection(this)) {
//            GetCatogoryTask getCatogoryTask = new GetCatogoryTask();
//            getCatogoryTask.execute();
            webServices();



        } else {
            networkCheckDialog();
        }
    }

    public void networkCheckDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setTitle("Network Connection");
        builder.setMessage("Please Check your internet connection.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.show();
    }


    public void webServices() {
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Loading ....");
//        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://designer321.com/rajohn/appname/api/getservices.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.e("RESPONSE", response);
                            JSONObject jobj = new JSONObject(response);
                            if (jobj != null) {
                                message_code = jobj.getString("message_code");

                                if (Integer.parseInt(message_code) == 1) {
                                    JSONArray responseArr = jobj.getJSONArray("response");
                                    if (responseArr.length() != 0) {
                                        for (int i = 0; i < responseArr.length(); i++) {
                                            int categoryId = responseArr.getJSONObject(i).getInt("category_id");
                                            String title = responseArr.getJSONObject(i).getString("title");
                                            String description = responseArr.getJSONObject(i).getString("description");
                                            String category_image = responseArr.getJSONObject(i).getString("category_image");

                                            imageData = new ImageData();
                                            imageData.setCategoryId(responseArr.getJSONObject(i).getInt("category_id"));
                                            imageData.setTitle(responseArr.getJSONObject(i).getString("title"));
                                            imageData.setCategory_image(responseArr.getJSONObject(i).getString("category_image"));
                                            imageData.setDescription(responseArr.getJSONObject(i).getString("description"));

                                            imageDataList.add(imageData);

                                            catIdList.add(categoryId);
                                            titleList.add(title);
                                            descriptionList.add(description);
                                            catImageList.add(category_image);
                                            WelcomeActivity.getList(catIdList, titleList, descriptionList, catImageList);
                                            db.insertData(imageData);
                                            handler();
                                        }
                                    } else {
                                        Toast.makeText(MainActivity.this, "Please Check Internet Connection", Toast.LENGTH_SHORT).show();
                                    }


                                } else {
                                    Toast.makeText(MainActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
                                }

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();

            }
        });
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                // Here goes the new timeout
                return 3000;
            }
            @Override
            public int getCurrentRetryCount() {
                // The max number of attempts
                return DefaultRetryPolicy.DEFAULT_MAX_RETRIES;
            }
            @Override
            public void retry(VolleyError error) throws VolleyError {
                // Here you could check if the retry count has gotten
                // To the max number, and if so, send a VolleyError msg
                // or something
                Toast.makeText(MainActivity.this, "Slow Internet Connection, please try again.", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);
    }
}
