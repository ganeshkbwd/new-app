package com.example.neha.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.neha.myapplication.model.ImageData;

/**
 * Created by neha on 11/26/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "appname";

    // Table Names
    private static final String TABLE_IMAGE = "imageText";

    // doctor Table - column names
    private static final String ID = "id";
    private static final String IMG_PATH = "path";
    private static final String TITLE = "title";
    private static final String DESC = "description";


    // developer table create statement
    private static final String CREATE_TABLE_IMAGE = "CREATE TABLE "
            + TABLE_IMAGE + "(" + ID + " INTEGER PRIMARY KEY, "
            + TITLE + " TEXT, " + IMG_PATH + " TEXT, " + DESC + " TEXT" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_IMAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // ------------------ReportMeta table
    public long insertData(ImageData imageData) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID, imageData.getCategoryId());
        values.put(TITLE, imageData.getTitle());
        values.put(IMG_PATH, imageData.getCategory_image());
        values.put(DESC, imageData.getDescription());


        // insert row
        long id = db.insert(TABLE_IMAGE, null, values);
        db.close();

        return id;
    }
}
