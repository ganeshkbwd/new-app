package com.example.neha.myapplication.adapter;

/**
 * Created by neha on 9/1/2016.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.neha.myapplication.R;
import com.example.neha.myapplication.WelcomeActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * View pager adapter
 */
public class MyViewPagerAdapter extends PagerAdapter {
    private LayoutInflater layoutInflater;
    Context context;
    List<String> imagesUrl;
    List<String> title;
    List<String> description;
    List<Integer> catId;

    public MyViewPagerAdapter(WelcomeActivity welcomeActivity, List<String> size, List<Integer> catId, List<String> title, List<String> description) {
        this.context = welcomeActivity;
        this.imagesUrl = size;
        this.catId = catId;
        this.description = description;
        this.title = title;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.welcome_slide1, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageurl);
//        TextView titleTV = (TextView) view.findViewById(R.id.title);
        TextView descriptionTV = (TextView) view.findViewById(R.id.description);

        Picasso.with(context).load(imagesUrl.get(position)).resize(350,300).into(imageView);
//        titleTV.setText(title.get(position) + "");
        descriptionTV.setText(description.get(position) + "");

        container.addView(view);


        return view;
    }

    @Override
    public int getCount() {

            return imagesUrl.size();

    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }


}
